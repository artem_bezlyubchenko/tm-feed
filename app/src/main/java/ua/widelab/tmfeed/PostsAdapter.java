package ua.widelab.tmfeed;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by mac on 30.01.15.
 */
public class PostsAdapter extends ArrayAdapter<PostModel> {

    LayoutInflater inflater;

    public PostsAdapter(Context context, PostModel[] objects) {
        super(context, R.layout.item_list, objects);
        inflater = LayoutInflater.from(context);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = inflater.inflate(R.layout.item_list, null);
        ImageView iv = (ImageView) v.findViewById(R.id.item_list_image);
        switch (getItem(position).getType()) {
            case habr:
                iv.setImageResource(R.drawable.habr);
                break;
            case mm:
                iv.setImageResource(R.drawable.megamozg);
                break;
            case gt:
                iv.setImageResource(R.drawable.geektimes);
                break;
            default:
        }
        ((TextView) v.findViewById(R.id.item_list_text)).setText(getItem(position).getTitle());
        ((TextView) v.findViewById(R.id.item_list_time)).setText(getItem(position).getTime_published());
        ((TextView) v.findViewById(R.id.item_list_comments)).setText(getItem(position).getComments_count()+"");
        ((TextView) v.findViewById(R.id.item_list_fav)).setText(getItem(position).getFavorites_count()+"");
        ((TextView) v.findViewById(R.id.item_list_reading)).setText(getItem(position).getReading_count()+"");

        return v;
    }
}
