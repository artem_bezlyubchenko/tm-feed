package ua.widelab.tmfeed;

import android.app.Activity;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.Arrays;
import java.util.List;

/**
 * Fragment used for managing interactions for and presentation of a navigation drawer.
 * See the <a href="https://developer.android.com/design/patterns/navigation-drawer.html#Interaction">
 * design guidelines</a> for a complete explanation of the behaviors implemented here.
 */
public class NavigationDrawerFragment extends Fragment {

    /**
     * Remember the position of the selected item.
     */
    private static final String STATE_SELECTED_POSITION = "selected_navigation_drawer_position";
    private static final String STATE_SELECTED_TMS = "selected_navigation_drawer_position_tms";
    private static final String STATE_SELECTED_DURATION = "selected_navigation_drawer_duration";

    /**
     * Per the design guidelines, you should show the drawer on launch until the user manually
     * expands it. This shared preference tracks this.
     */
    private static final String PREF_USER_LEARNED_DRAWER = "navigation_drawer_learned";

    /**
     * A pointer to the current callbacks instance (the Activity).
     */
    private NavigationDrawerCallbacks mCallbacks;

    /**
     * Helper component that ties the action bar to the navigation drawer.
     */
    private ActionBarDrawerToggle mDrawerToggle;

    private DrawerLayout mDrawerLayout;
    private ListView mTypeListView, mTmListView, mDurationListView;
    private View mFragmentContainerView;

    private int mCurrentSelectedTypePosition = 0;
    private int mCurrentSelectedDurationPosition = 0;
    private boolean[] isSelected;
    private boolean mFromSavedInstanceState;
    private boolean mUserLearnedDrawer;
    private boolean changed;

    public NavigationDrawerFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Read in the flag indicating whether or not the user has demonstrated awareness of the
        // drawer. See PREF_USER_LEARNED_DRAWER for details.
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getActivity());
        mUserLearnedDrawer = sp.getBoolean(PREF_USER_LEARNED_DRAWER, false);

        if (savedInstanceState != null) {
            mCurrentSelectedTypePosition = savedInstanceState.getInt(STATE_SELECTED_POSITION);
            mCurrentSelectedDurationPosition = savedInstanceState.getInt(STATE_SELECTED_DURATION);
            isSelected = savedInstanceState.getBooleanArray(STATE_SELECTED_TMS);
            mFromSavedInstanceState = true;
        } else {
            isSelected = new boolean[3];
            if (sp.contains(STATE_SELECTED_TMS))
            {
                String str = sp.getString(STATE_SELECTED_TMS,"111");
                for (int i=0;i< str.length();i++){
                    isSelected[i] = (str.charAt(i) == '1');
                }
            }
            else {
                Arrays.fill(isSelected, true);
            }
            mCurrentSelectedDurationPosition = sp.getInt(STATE_SELECTED_DURATION, 0);
            mCurrentSelectedTypePosition = sp.getInt(STATE_SELECTED_POSITION, 0);

        }
        // Select either the default item (0) or the last selected item.
        selectType(mCurrentSelectedTypePosition);

        refresh();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        // Indicate that this fragment would like to influence the set of actions in the action bar.
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(
                R.layout.fragment_navigation_drawer, container, false);


        mTypeListView = (ListView) v.findViewById(R.id.list_view_type);
        mTypeListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                PreferenceManager.getDefaultSharedPreferences(getActivity()).edit().putInt(STATE_SELECTED_POSITION, position).apply();
                selectType(position);
            }
        });
        mTypeListView.setAdapter(new ArrayAdapter<String>(
                getActionBar().getThemedContext(),
                android.R.layout.simple_list_item_activated_1,
                android.R.id.text1,
                getResources().getStringArray(R.array.type)));
        mTypeListView.setItemChecked(mCurrentSelectedTypePosition, true);

        mTmListView = (ListView) v.findViewById(R.id.list_view_tm);
        mTmListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                selectTm(position);
            }
        });
        mTmListView.setAdapter(new ArrayAdapter<String>(
                getActionBar().getThemedContext(),
                android.R.layout.simple_list_item_checked,
                android.R.id.text1,
                getResources().getStringArray(R.array.tm)));
        for (int i = 0; i < isSelected.length; i++) {
            if (isSelected[i]) {
                mTmListView.setItemChecked(i, true);
            }
        }
        mDurationListView = (ListView) v.findViewById(R.id.list_view_duration);
        mDurationListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                PreferenceManager.getDefaultSharedPreferences(getActivity()).edit().putInt(STATE_SELECTED_DURATION, position).apply();
                selectDuration(position);
            }
        });
        mDurationListView.setAdapter(new ArrayAdapter<String>(
                getActionBar().getThemedContext(),
                android.R.layout.simple_list_item_activated_1,
                android.R.id.text1,
                getResources().getStringArray(R.array.duration)));
        mDurationListView.setItemChecked(mCurrentSelectedDurationPosition, true);
        mDurationListView.setVisibility(mCurrentSelectedTypePosition==0?View.VISIBLE:View.INVISIBLE);
        return v;
    }

    public boolean isDrawerOpen() {
        return mDrawerLayout != null && mDrawerLayout.isDrawerOpen(mFragmentContainerView);
    }

    /**
     * Users of this fragment must call this method to set up the navigation drawer interactions.
     *
     * @param fragmentId   The android:id of this fragment in its activity's layout.
     * @param drawerLayout The DrawerLayout containing this fragment's UI.
     */
    public void setUp(int fragmentId, DrawerLayout drawerLayout) {
        mFragmentContainerView = getActivity().findViewById(fragmentId);
        mDrawerLayout = drawerLayout;

        // set a custom shadow that overlays the main content when the drawer opens
        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
        // set up the drawer's list view with items and click listener

        ActionBar actionBar = getActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);

        // ActionBarDrawerToggle ties together the the proper interactions
        // between the navigation drawer and the action bar app icon.
        mDrawerToggle = new ActionBarDrawerToggle(
                getActivity(),                    /* host Activity */
                mDrawerLayout,                    /* DrawerLayout object */
                R.drawable.ic_drawer,             /* nav drawer image to replace 'Up' caret */
                R.string.navigation_drawer_open,  /* "open drawer" description for accessibility */
                R.string.navigation_drawer_close  /* "close drawer" description for accessibility */
        ) {
            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                if (!isAdded()) {
                    return;
                }

                getActivity().supportInvalidateOptionsMenu(); // calls onPrepareOptionsMenu()
                if (changed) {
                    refresh();
                    changed = false;
                }
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                if (!isAdded()) {
                    return;
                }

                if (!mUserLearnedDrawer) {
                    // The user manually opened the drawer; store this flag to prevent auto-showing
                    // the navigation drawer automatically in the future.
                    mUserLearnedDrawer = true;
                    SharedPreferences sp = PreferenceManager
                            .getDefaultSharedPreferences(getActivity());
                    sp.edit().putBoolean(PREF_USER_LEARNED_DRAWER, true).apply();
                }

                getActivity().supportInvalidateOptionsMenu(); // calls onPrepareOptionsMenu()
            }
        };

        // If the user hasn't 'learned' about the drawer, open it to introduce them to the drawer,
        // per the navigation drawer design guidelines.
        if (!mUserLearnedDrawer && !mFromSavedInstanceState) {
            mDrawerLayout.openDrawer(mFragmentContainerView);
        }

        // Defer code dependent on restoration of previous instance state.
        mDrawerLayout.post(new Runnable() {
            @Override
            public void run() {
                mDrawerToggle.syncState();
            }
        });

        mDrawerLayout.setDrawerListener(mDrawerToggle);
    }

    private void selectType(int position) {
        if (getView()==null)
            return;
        try {
            if (mDurationListView == null)
                mDurationListView = (ListView) getView().findViewById(R.id.list_view_duration);
            if (mTypeListView == null)
                mTypeListView = (ListView) getView().findViewById(R.id.list_view_type);

            if (mCurrentSelectedTypePosition != position)
                changed = true;

            if (position != 0)
                mDurationListView.setVisibility(View.INVISIBLE);
            else if (mDurationListView != null)
                mDurationListView.setVisibility(View.VISIBLE);

            mCurrentSelectedTypePosition = position;
            if (mTypeListView != null) {
                mTypeListView.setItemChecked(position, true);
            }
            if (mDrawerLayout != null) {
                mDrawerLayout.closeDrawer(mFragmentContainerView);
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        /*if (mCallbacks != null) {
            mCallbacks.onNavigationDrawerItemSelected(position);
        }*/
    }

    private void selectTm(int position) {
        changed = true;
        isSelected[position] = !isSelected[position];
        String selected = "";
        for (int i=0;i<isSelected.length;i++)
        {
            selected+=isSelected[i]?"1":"0";
        }
        PreferenceManager.getDefaultSharedPreferences(getActivity()).edit().putString(STATE_SELECTED_TMS,selected).apply();
    }

    private void selectDuration(int position) {
        if (mCurrentSelectedDurationPosition != position)
            changed = true;
        mCurrentSelectedDurationPosition = position;
        if (mDrawerLayout != null) {
            mDrawerLayout.closeDrawer(mFragmentContainerView);
        }
    }

    private void refresh() {
        mCallbacks.update(mCurrentSelectedTypePosition, isSelected, mCurrentSelectedDurationPosition);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mCallbacks = (NavigationDrawerCallbacks) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException("Activity must implement NavigationDrawerCallbacks.");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mCallbacks = null;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(STATE_SELECTED_POSITION, mCurrentSelectedTypePosition);
        outState.putBooleanArray(STATE_SELECTED_TMS, isSelected);
        outState.putInt(STATE_SELECTED_DURATION, mCurrentSelectedDurationPosition);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Forward the new configuration the drawer toggle component.
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // If the drawer is open, show the global app actions in the action bar. See also
        // showGlobalContextActionBar, which controls the top-left area of the action bar.
        if (mDrawerLayout != null && isDrawerOpen()) {
            inflater.inflate(R.menu.global, menu);
            showGlobalContextActionBar();
        }
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }


        return super.onOptionsItemSelected(item);
    }

    /**
     * Per the navigation drawer design guidelines, updates the action bar to show the global app
     * 'context', rather than just what's in the current screen.
     */
    private void showGlobalContextActionBar() {
        ActionBar actionBar = getActionBar();
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setTitle(R.string.app_name);
    }

    private ActionBar getActionBar() {
        return ((ActionBarActivity) getActivity()).getSupportActionBar();
    }

    /**
     * Callbacks interface that all activities using this fragment must implement.
     */
    public static interface NavigationDrawerCallbacks {
        /**
         * Called when an item in the navigation drawer is selected.
         */
        void update(int position, boolean[] tms, int duration);
    }
}
