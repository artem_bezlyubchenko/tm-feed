package ua.widelab.tmfeed;

import android.os.AsyncTask;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by mac on 30.01.15.
 */
public class Request extends AsyncTask<String, Void, PostModel[]> {

    MainActivity activity;

    public Request(MainActivity activity) {
        this.activity = activity;
    }


    @Override
    protected PostModel[] doInBackground(String... params) {

        HttpClient client = new DefaultHttpClient();
        HttpGet request = new HttpGet(params[0]);

        try {
            HttpResponse response = client.execute(request);
            BufferedReader rd = new BufferedReader(
                    new InputStreamReader(response.getEntity().getContent()));

            StringBuffer result = new StringBuffer();
            String line = "";
            while ((line = rd.readLine()) != null) {
                result.append(line);
            }
            System.out.print(result.toString());
            JSONArray jar = new JSONArray(result.toString());
            PostModel[] posts = new PostModel[jar.length()];
            for (int i = 0; i < posts.length; i++) {
                posts[i] = new PostModel(jar.getJSONObject(i));
            }
            return posts;
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(PostModel[] posts) {
        super.onPostExecute(posts);
        activity.update(posts);
    }
}
