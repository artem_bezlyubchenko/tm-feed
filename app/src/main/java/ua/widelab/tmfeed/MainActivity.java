package ua.widelab.tmfeed;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.ListFragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;


public class MainActivity extends ActionBarActivity
        implements NavigationDrawerFragment.NavigationDrawerCallbacks {

    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    private NavigationDrawerFragment mNavigationDrawerFragment;

    /**
     * Used to store the last screen title. For use in {@link #restoreActionBar()}.
     */
    private CharSequence mTitle;

    private ListFragment fragment;

    PostModel[] posts;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);
        //mTitle = getString(R.string.best);

        // Set up the drawer.
        mNavigationDrawerFragment.setUp(
                R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout));
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragment = new PostsFragment();
        fragmentManager.beginTransaction().replace(R.id.container, fragment).commit();
    }

    public void onSectionAttached(int number) {
        mTitle = getResources().getStringArray(R.array.type)[number];
    }

    public void restoreActionBar() {
        ActionBar actionBar = getSupportActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(mTitle);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!mNavigationDrawerFragment.isDrawerOpen()) {
            // Only show items in the action bar relevant to this screen
            // if the drawer is not showing. Otherwise, let the drawer
            // decide what to show in the action bar.
            restoreActionBar();
            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public void update(int position, boolean[] tms, int duration) {
        onSectionAttached(position);
        if (fragment != null && fragment.getListAdapter() != null) {
            posts = null;
            fragment.setListShown(false);
        }
        new Request(this).execute(makeRequest(position, tms, duration));
    }

    public String makeRequest(int position, boolean[] tms, int duration) {
        StringBuilder sb = new StringBuilder();
        sb.append("http://tmfeed.ru/posts/");
        boolean used = false;
        for (int i = 0; i < tms.length; i++) {
            if (tms[i]) {
                if (used)
                    sb.append("-");

                switch (i) {
                    case 0:
                        sb.append("habrahabr");
                        break;
                    case 1:
                        sb.append("megamozg");
                        break;
                    case 2:
                        sb.append("geektimes");
                        break;
                    default:
                }
                used = true;

            }
        }

        sb.append("_");

        switch (position) {
            case 0:
                sb.append("top");
                break;
            case 1:
                sb.append("interesting");
                break;
            case 2:
                sb.append("all");
                break;
            default:
        }

        sb.append("_");

        if (duration == 3 || position != 0) {
            sb.append("alltime");
        } else {
            switch (duration) {
                case 0:
                    sb.append("daily");
                    break;
                case 1:
                    sb.append("weekly");
                    break;
                case 2:
                    sb.append("monthly");
                    break;
                default:
            }
        }

        sb.append(".json");
        return sb.toString();
    }

    public void update(PostModel[] posts) {
        if (posts == null || posts.length == 0) {
            Toast.makeText(this, "Some error", Toast.LENGTH_LONG).show();
        } else {
            fragment.setListAdapter(new PostsAdapter(this, posts));
            fragment.setListShown(true);
        }
    }


    public static class PostsFragment extends ListFragment {
        @Override
        public void onListItemClick(ListView l, View v, int position, long id) {
            super.onListItemClick(l, v, position, id);
            Intent intent = new Intent(getActivity(), PostActivity.class);
            intent.putExtra("url", ((PostModel) getListAdapter().getItem(position)).getUrl());
            startActivity(intent);
        }
    }

}
