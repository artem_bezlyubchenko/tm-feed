package ua.widelab.tmfeed;

import android.util.Log;

import org.json.JSONObject;

import java.util.Date;
import java.text.SimpleDateFormat;

/**
 * Created by mac on 30.01.15.
 */
public class PostModel {
    enum Site {habr, mm, gt}

    private String url;
    private String title;
    private Date time_published;
    private int comments_count, reading_count, favorites_count;

    public int getComments_count() {
        return comments_count;
    }

    public void setComments_count(int comments_count) {
        this.comments_count = comments_count;
    }

    public int getReading_count() {
        return reading_count;
    }

    public void setReading_count(int reading_count) {
        this.reading_count = reading_count;
    }

    public int getFavorites_count() {
        return favorites_count;
    }

    public void setFavorites_count(int favorites_count) {
        this.favorites_count = favorites_count;
    }

    private Site type;

    public PostModel(JSONObject json) {
        setUrl(json.optString("url"));
        setTitle(json.optString("title"));
        setType(json.optString("site"));
        setTime_published(json.optString("time_published"));
        setComments_count(json.optInt("comments_count"));
        setReading_count(json.optInt("reading_count"));
        setFavorites_count(json.optInt("favorites_count"));
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

     public String getTime_published() {
         try {
             return new SimpleDateFormat("HH:mm dd.MM").format(time_published);
         }
         catch (Exception e)
         {
             return  "";
         }
     }

    public void setTime_published(String time_published) {
        try {
            this.time_published = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ").parse(time_published);
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    public Site getType() {
        return type;
    }

    public void setType(String site) {
        if (site.equals("habrahabr"))
            type = Site.habr;
        else if (site.equals("megamozg"))
            type = Site.mm;
        else
            type = Site.gt;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url.replace("http://","http://m.");

    }
}
